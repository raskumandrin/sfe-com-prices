#!/usr/bin/perl

use strict;

use LWP::UserAgent;
use HTML::TokeParser;

$| = 1;

my $ua = LWP::UserAgent->new;
$ua->timeout(10);
$ua->env_proxy;
 
# Get link list

my %links;

my $url_base = 'http://www.sfe.com.au/content/prices/';

my $response = $ua->get("${url_base}rtp15SFBN.html");
 
if ($response->is_success) {
    my $html = $response->decoded_content;
#   print $response->decoded_content;  # or whatever
my $p = HTML::TokeParser->new(\$html);
    while ( my $token = $p->get_token ) {
        # This prints all text in an HTML doc (i.e., it strips the HTML)
        if ( $token->[0] eq 'S' and $token->[1] eq 'option' ) {
            my $url = $token->[2]->{value};
            my $token_next = $p->get_token;
            my $title = $token_next->[1];
            $title =~ s/ASX Electricity //;
            $title =~ s/\$/USD-/;
            $title =~ s/\r//g;
            $title =~ s/\n//g;
            $links{ $url } = $title;
     #       print  "$url\t$title\n";
        }
    }
}
else {
    die $response->status_line;
}

my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime(time);
$mon += 1;
$mon = "0$mon" unless $mon =~ /\d\d/;
$year += 1900;
$mday = "0$mday" unless $mday =~ /\d\d/;
$hour = "0$hour" unless $hour =~ /\d\d/;
$min = "0$min" unless $min =~ /\d\d/;
my $folder = "$year-$mon-$mday--$hour-$min";

mkdir "$folder";

foreach my $url (keys %links) {
    my $response = $ua->get("${url_base}$url");
    print $links{$url}."\n";
    if ($response->is_success) {
        open(my $fh, '>', "$folder/".$links{$url}.".csv");
        my $html = $response->decoded_content;
    #   print $response->decoded_content;  # or whatever
        my $p = HTML::TokeParser->new(\$html);
        
        
        while ( my $token = $p->get_token ) {
            # This prints all text in an HTML doc (i.e., it strips the HTML)
            if ( $token->[0] eq 'S' and $token->[1] eq 'tr' and $token->[2] and $token->[2]->{class} and ( $token->[2]->{class} eq 'noHighlight' or $token->[2]->{class} eq 'Highlight' ) ) {
                $p->get_token;
                my $td_1_token = $p->get_token;
                my $expire = $td_1_token->[1];
                my ($month,$year) = ( $expire =~ /^(.+)\s(.+)$/ );
                $year = "20$year";
                
                $p->get_token foreach(1..32);
                my $td_12_token = $p->get_token;
                my $settlement = $td_12_token->[1];
                print $fh "$year\t$month\t$settlement\n"
            }
        }    
        close $fh;
    }
    else {
        warn $response->status_line;
    }

}

__END__

Could you please quote me to create an exe that will query the following website. 
http://www.sfe.com.au/content/prices/rtp15SFBN.html
The program will need to select all relative dropdown options and export them to an excel
or csv separated by the name of each table. We will only need the first and last columns.
That is..."Expiry" and "Previous Settlement" columns.